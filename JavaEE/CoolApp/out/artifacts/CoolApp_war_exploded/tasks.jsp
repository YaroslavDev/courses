<%@ page import="learning.entity.Task" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="learning.entity.Person" %>
<%@ page import="java.util.List" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String personName = (String)request.getAttribute("person");
    List<Task> tasks = (List<Task>)request.getAttribute("tasks");
%>
<html>
<head>
    <title></title>
</head>
<body>
<h1><%=personName%>'s tasks: </h1>
<table>
    <tr>
        <th>Task ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Owner ID</th>
    </tr>
    <%
        for (Task t : tasks) {
    %>
    <tr>
        <td><%=t.gettID()%></td>
        <td><%=t.gettTitle()%></td>
        <td><%=t.gettDescription()%></td>
        <td><%=t.getpID()%></td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
