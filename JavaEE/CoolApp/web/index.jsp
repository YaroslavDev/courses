
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title></title>
  </head>
  <body>
    <h1>Welcome to CoolApp!</h1>
    <h2>Available API:</h2>
    <div id="API">
        <ul>
            <li>/persons</li>
            <li>/tasks?person=Yaroslav</li>
        </ul>
    </div>
  </body>
</html>
