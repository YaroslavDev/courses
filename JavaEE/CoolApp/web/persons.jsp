<%@ page import="learning.entity.Person" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: ciupin-iaroslav
  Date: 18/10/14
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<h1>All persons:</h1>
<ul>
<%
    List<Person> persons = (List<Person>)request.getAttribute("persons");
    for (Person p : persons) {
        %>
        <li><%=p.getpName()%></li>
        <%
    }
%>
</ul>

</body>
</html>
