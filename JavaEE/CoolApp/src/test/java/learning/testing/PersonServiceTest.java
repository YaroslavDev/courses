package learning.testing;

import learning.crud.PersonService;
import learning.entity.Person;
import org.junit.Test;

import java.util.List;

public class PersonServiceTest {

    PersonService service = new PersonService();

    @Test
    public void testSaveAndDeleteRecord() throws Exception {
        Person p = new Person();
        p.setpName("FOO");

        Person person = service.add(p);

        System.out.println(person);

        service.delete(person.getpID());
    }

    @Test
    public void testSelectRecord() throws Exception {
        Person p = new Person();
        p.setpName("BAR");

        Person personFromDB = service.add(p);

        Person selectedPerson = service.get(personFromDB.getpID());

        System.out.println(selectedPerson);

        service.delete(selectedPerson.getpID());
    }

    @Test
    public void testUpdate() throws Exception {
        Person p = new Person();
        p.setpName("FOOBAR");

        p = service.add(p);
        p.setpName("BARFOO");
        service.update(p);

        Person p2 = service.get(p.getpID());
        System.out.println(p2);

        service.delete(p.getpID());
    }

    @Test
    public void testGetAll() {
        Person p1 = new Person();
        p1.setpName("Foo");

        Person p2 = new Person();
        p2.setpName("Bar");

        Person p3 = new Person();
        p3.setpName("FooBar");

        p1 = service.add(p1);
        p2 = service.add(p2);
        p3 = service.add(p3);

        List<Person> persons = service.getAll();

        System.out.println("ALL PERSONS: ");
        for (Person p : persons) {
            System.out.println(p);
        }

        service.delete(p1.getpID());
        service.delete(p2.getpID());
        service.delete(p3.getpID());
    }
}
