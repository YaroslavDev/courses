package learning.testing;


import learning.crud.TaskService;
import learning.entity.Task;
import org.junit.Test;

import java.util.List;

public class TaskServiceTest {

    TaskService service = new TaskService();

    @Test
    public void testSaveAndDeleteRecord() throws Exception {
        Task t = new Task(1, "FooTask", "FooDescription", 1);

        Task taskFromDB = service.add(t);

        System.out.println(taskFromDB);

        service.delete(taskFromDB.gettID());
    }

    @Test
    public void testSelectRecord() throws Exception {
        Task t = new Task(1, "FooTask", "FooDescription", 1);

        Task taskFromDB = service.add(t);

        Task selectedTask = service.get(taskFromDB.gettID());

        System.out.println(selectedTask);

        service.delete(selectedTask.gettID());
    }

    @Test
    public void testUpdate() throws Exception {
        Task t = new Task(1, "FooTask", "FooDescription", 1);

        t = service.add(t);
        t.settTitle("BarTask");
        t.settDescription("BarDescription");
        service.update(t);

        Task t2 = service.get(t.gettID());
        System.out.println(t2);

        service.delete(t.gettID());
    }

    @Test
    public void testGetAll() {
        Task t1 = new Task(1, "FooTask", "FooDescription", 1);
        Task t2 = new Task(2, "BarTask", "BarDescription", 1);
        Task t3 = new Task(3, "FooBarTask", "FooBarDescription", 1);

        t1 = service.add(t1);
        t2 = service.add(t2);
        t3 = service.add(t3);

        List<Task> tasks = service.getAll();

        System.out.println("ALL TASKS: ");
        for (Task t : tasks) {
            System.out.println(t);
        }

        service.delete(t1.gettID());
        service.delete(t2.gettID());
        service.delete(t3.gettID());
    }
}
