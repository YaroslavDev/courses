package learning;

import learning.crud.PersonService;
import learning.crud.TaskService;
import learning.entity.Person;
import learning.entity.Task;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TasksServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String JDBC_DRIVER="com.mysql.jdbc.Driver";
        final String DB_URL="jdbc:mysql://localhost:3306/mydb";
        final String USER = "root";
        final String PASS = "toor";

        PersonService personService = new PersonService();
        TaskService taskService = new TaskService();

        String personName= req.getParameter("person");
        req.setAttribute("person", personName);

        Person p = personService.getByName(personName);
        List<Task> personTasks = taskService.getPersonTasks(p.getpID());
        req.setAttribute("tasks", personTasks);
        //try {

            //Class.forName(JDBC_DRIVER);
            //Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);

            //Statement statement = connection.createStatement();
            //String sqlQuery = "SELECT * FROM person JOIN task ON person.pID = task.person_pID WHERE pName = '"
            //        + personName + "'";
//            ResultSet rs = statement.executeQuery(sqlQuery);
//
//            while (rs.next()) {
//                Task t = new Task(rs.getInt("tID"),
//                        rs.getString("tTitle"),
//                        rs.getString("tDescription"),
//                        rs.getInt("person_pID"));
//                System.out.println(t);
//                tasks.add(t);
//            }
//
            //req.setAttribute("tasks", tasks);
//
//            rs.close();
//            statement.close();
//            connection.close();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        getServletContext().getRequestDispatcher("/tasks.jsp").forward(req, resp);
    }
}
