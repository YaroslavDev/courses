package learning.entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "person")
@NamedQuery(name = "Person.getAll", query = "SELECT c FROM Person c")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pID")
    protected long pID;

    @Column
    protected String pName;

    //protected ArrayList<Task> tasks = new ArrayList<>();

    public Person() {
    }

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
//    public ArrayList<Task> getTasks() {
//        return tasks;
//    }
//
//    public void setTasks(ArrayList<Task> tasks) {
//        this.tasks = tasks;
//    }

    public Person(long id, String name) {
        pID = id;

        pName = name;
    }

    public long getpID() {
        return pID;
    }

    public void setpID(long pID) {
        this.pID = pID;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    @Override
    public String toString() {
        return "(" + pID + "," + pName + ")";
    }
}
