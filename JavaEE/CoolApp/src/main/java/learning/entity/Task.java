package learning.entity;

import javax.persistence.*;

@Entity
@Table(name = "task")
@NamedQuery(name = "Task.getAll", query = "select c from Task c")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tID")
    protected long tID;

    @Column(name = "tTitle")
    protected String tTitle;

    @Column(name = "tDescription")
    protected String tDescription;

    @Column(name = "pID")
    protected long pID;

//    protected Person owner;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "pID", nullable = false)
//    public Person getOwner() {
//        return owner;
//    }
//
//    public void setOwner(Person owner) {
//        this.owner = owner;
//    }

    public Task() {
    }

    public Task(long id, String title, String desc, long owner) {
        tID = id;
        tTitle = title;
        tDescription = desc;
        pID = owner;
    }

    public long gettID() {
        return tID;
    }

    public void settID(long tID) {
        this.tID = tID;
    }

    public String gettTitle() {
        return tTitle;
    }

    public void settTitle(String tTitle) {
        this.tTitle = tTitle;
    }

    public String gettDescription() {
        return tDescription;
    }

    public void settDescription(String tDescription) {
        this.tDescription = tDescription;
    }

    public long getpID() {
        return pID;
    }

    public void setpID(long pID) {
        this.pID = pID;
    }

    @Override
    public String toString() {
        return "(" + tID + "," + tTitle + "," + tDescription + "," + pID + ")";
    }
}
