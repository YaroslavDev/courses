package learning;

import learning.crud.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class PersonsServlet extends HttpServlet {
    private PersonService service = new PersonService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("persons", service.getAll());
        getServletContext().getRequestDispatcher("/persons.jsp").forward(req, resp);
    }
}
