package learning.crud;

import learning.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class TaskService {
    public EntityManager em = Persistence.createEntityManagerFactory("CoolApp").createEntityManager();

    public Task add(Task t) {
        em.getTransaction().begin();
        Task taskFromDB = em.merge(t);
        em.getTransaction().commit();
        return taskFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Task get(long id){
        return em.find(Task.class, id);
    }

    public void update(Task t){
        em.getTransaction().begin();
        em.merge(t);
        em.getTransaction().commit();
    }

    public List<Task> getAll(){
        return em.createNamedQuery("Task.getAll", Task.class)
                .getResultList();
    }

    public List<Task> getPersonTasks(long pID) {
        return em.createQuery("SELECT c FROM Task c WHERE pID = :persId")
                .setParameter("persId", pID)
                .getResultList();
    }
}
