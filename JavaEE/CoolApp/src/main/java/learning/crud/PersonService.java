package learning.crud;

import learning.entity.Person;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonService {
    public EntityManager em = Persistence.createEntityManagerFactory("CoolApp").createEntityManager();

    public Person add(Person t) {
        em.getTransaction().begin();
        Person PersonFromDB = em.merge(t);
        em.getTransaction().commit();
        return PersonFromDB;
    }

    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Person get(long id){
        return em.find(Person.class, id);
    }

    public Person getByName(String name) {
        return (Person)em.createQuery("SELECT c FROM Person c WHERE pName LIKE :persName")
                .setParameter("persName", name)
                .getResultList().get(0);
    }

    public void update(Person t){
        em.getTransaction().begin();
        em.merge(t);
        em.getTransaction().commit();
    }

    public List<Person> getAll(){
        TypedQuery<Person> namedQuery = em.createNamedQuery("Person.getAll", Person.class);
        return namedQuery.getResultList();
    }
}
