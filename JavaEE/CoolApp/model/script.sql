insert into person values (1, "Yaroslav");
insert into person values (2, "John");
insert into person values (3, "Yamamoto");

insert into task values (1, "JPA + Hibernate", "JavaEE", 1);
insert into task values (2, "Essential classes", "JavaSE", 1);

insert into task values (3, "Tomorrow 9.15am", "Hiking", 2);
insert into task values (4, "Today 19.00pm", "Cycling", 2);

insert into task values (5, "my favourite", "Nuclear physics", 3);
insert into task values (6, "DNA mining", "Bioinformatics", 3);

select *
from person natural join task
where pID = 2;