package com.endava.taskapp.data.repository

import com.endava.taskapp.data.config.DataHibernateIntegrationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = DataHibernateIntegrationConfig.class)
class PersonRepositoryHibernateIntegrationSpec extends PersonRepositoryIntegrationSpec {

    @Autowired
    PersonRepository personRepositoryImpl

    def setup() {
        personRepository = personRepositoryImpl
    }
}
