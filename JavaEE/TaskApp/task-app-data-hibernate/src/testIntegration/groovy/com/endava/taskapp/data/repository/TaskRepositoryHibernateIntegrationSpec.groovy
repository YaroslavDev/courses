package com.endava.taskapp.data.repository

import com.endava.taskapp.data.config.DataHibernateIntegrationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = DataHibernateIntegrationConfig.class)
class TaskRepositoryHibernateIntegrationSpec extends TaskRepositoryIntegrationSpec {

    @Autowired
    PersonRepository personRepositoryImpl

    @Autowired
    TaskRepository taskRepositoryImpl

    def setup() {
        personRepository = personRepositoryImpl
        taskRepository = taskRepositoryImpl
    }
}
