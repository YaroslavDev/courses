package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Person;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private static final Logger logger = LoggerFactory.getLogger(PersonRepositoryImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Person addPerson(Person person) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(person);
        transaction.commit();

        session.close();

        logger.info("Person saved succesfully, Person details = " + person);

        return person;
    }

    @Override
    public Person getPersonById(long id) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Person person = (Person) session.get(Person.class, new Long(id));
        transaction.commit();

        session.close();

        logger.info("Person loaded succesfully, Person details = " + person);

        return person;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Person> listPersons() {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        List<Person> personList = session.createQuery("from Person").list();
        transaction.commit();

        session.close();

        logger.info("Person list loaded succesfully: ");

        for (Person person : personList) {
            logger.info(person.toString());
        }

        return personList;
    }

    @Override
    public Person removePerson(long id) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Person person = (Person) session.get(Person.class, new Long(id));

        try {
            session.delete(person);
            logger.info("Person deleted succesfully, person details = " + person);
        } catch (ObjectNotFoundException ex) {
            logger.info("Person was not found in database");
        } finally {
            transaction.commit();
            session.close();
        }

        return person;
    }

    @Override
    public Person updatePerson(Person person) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.update(person);
        transaction.commit();

        session.close();

        logger.info("Person updated succesfully, Person details = " + person);

        return person;
    }
}