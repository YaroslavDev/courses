package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Task;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    private static final Logger logger = LoggerFactory.getLogger(TaskRepositoryImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Task addTask(Task task) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(task);
        transaction.commit();

        session.close();

        logger.info("Task saved succesfully, Task details = " + task);

        return task;
    }

    public Task getTaskById(long id) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Task task = (Task) session.get(Task.class, new Long(id));
        transaction.commit();

        session.close();

        logger.info("Task loaded succesfully, Task details = " + task);

        return task;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> listTasks() {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        List<Task> taskList = session.createQuery("from Task").list();
        transaction.commit();

        session.close();

        logger.info("Task list loaded succesfully: ");

        for (Task task : taskList) {
            logger.info(task.toString());
        }

        return taskList;
    }

    @Override
    public Task removeTask(long id) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Task task = (Task) session.get(Task.class, new Long(id));

        try {
            session.delete(task);
            logger.info("Task deleted succesfully, Task details = " + task);
        } catch (ObjectNotFoundException ex) {
            logger.info("Task was not found in database");
        } finally {
            transaction.commit();
            session.close();
        }

        return task;
    }

    @Override
    public Task updateTask(Task task) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.update(task);
        transaction.commit();

        session.close();

        logger.info("Task updated succesfully, Task details = " + task);

        return task;
    }
}
