package com.endava.taskapp.data.repository

import com.endava.taskapp.data.model.Person
import org.hibernate.Query
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.Transaction
import spock.lang.Specification

class PersonRepositoryUnitSpec extends Specification {

    PersonRepository personRepository
    SessionFactory sessionFactory
    Session session
    Transaction transaction

    def setup() {
        session = Mock()
        transaction = Mock()
        sessionFactory = Mock()
        personRepository = new PersonRepositoryImpl(sessionFactory: sessionFactory)
    }

    def "addPerson should add new Person to database"() {
        setup:
        Person newPerson = new Person(1, "John")

        when:
        Person savedPerson = personRepository.addPerson(newPerson)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.persist(newPerson)
        1 * transaction.commit()
        1 * session.close()

        expect:
        newPerson.equals(savedPerson)
    }

    def "getPersonById should return Person with specified id"() {
        setup:
        Person expectedPerson = new Person(1, "John")

        when:
        Person actualPerson = personRepository.getPersonById(expectedPerson.id)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.get(Person.class, expectedPerson.id) >> expectedPerson
        1 * transaction.commit()
        1 * session.close()

        expect:
        actualPerson.equals(expectedPerson)
    }

    def "listPersons should return all Persons from database"() {
        setup:
        Person expectedPerson = new Person(1, "John")
        Query query = Mock()

        when:
        List<Person> actualPersonList = personRepository.listPersons()

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.createQuery("from Person") >> query
        1 * query.list() >> [expectedPerson]
        1 * transaction.commit()
        1 * session.close()

        expect:
        actualPersonList.size() == 1
        actualPersonList.first().equals(expectedPerson)
    }

    def "removePerson should remove Person from database"() {
        setup:
        long id = 1
        long nonExistentId = 666
        Person expectedPerson = new Person(1, "John")

        when:
        Person existentPerson = personRepository.removePerson(id)
        Person nonExistentPerson = personRepository.removePerson(nonExistentId)

        then:
        2 * sessionFactory.openSession() >> session
        2 * session.beginTransaction() >> transaction
        1 * session.get(Person.class, id) >> expectedPerson
        1 * session.get(Person.class, nonExistentId) >> null
        1 * session.delete(expectedPerson)
        2 * transaction.commit()
        2 * session.close()

        expect:
        existentPerson.equals(expectedPerson)
        nonExistentPerson == null
    }

    def "updatePerson should update existing Person in database"() {
        setup:
        Person person = new Person(1, "John")

        when:
        Person updatedPerson = personRepository.updatePerson(person)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.update(person)
        1 * transaction.commit()
        1 * session.close()

        expect:
        updatedPerson.equals(person)
    }
}
