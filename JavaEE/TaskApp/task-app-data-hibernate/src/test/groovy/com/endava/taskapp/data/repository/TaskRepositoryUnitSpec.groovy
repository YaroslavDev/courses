package com.endava.taskapp.data.repository

import com.endava.taskapp.data.config.DataHibernateIntegrationConfig
import com.endava.taskapp.data.model.Person
import com.endava.taskapp.data.model.Task
import org.hibernate.Query
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.Transaction
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = DataHibernateIntegrationConfig.class)
class TaskRepositoryUnitSpec extends Specification {

    TaskRepository taskRepository
    SessionFactory sessionFactory
    Session session
    Transaction transaction

    def setup() {
        session = Mock()
        transaction = Mock()
        sessionFactory = Mock()
        taskRepository = new TaskRepositoryImpl(sessionFactory: sessionFactory)
    }

    def "addTask should add new Task to application"() {
        setup:
        Task newTask = new Task(1, "A", "XXX", null)

        when:
        Task savedTask = taskRepository.addTask(newTask)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.persist(newTask)
        1 * transaction.commit()
        1 * session.close()

        expect:
        newTask.equals(savedTask)
    }

    def "getTaskById should return Task with specified id"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        Task actualTask = taskRepository.getTaskById(expectedTask.id)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.get(Task.class, expectedTask.id) >> expectedTask
        1 * transaction.commit()
        1 * session.close()

        expect:
        actualTask.equals(expectedTask)
    }

    def "listTasks should return all Tasks from application"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)
        Query query = Mock()

        when:
        List<Person> actualTaskList = taskRepository.listTasks()

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.createQuery("from Task") >> query
        1 * query.list() >> [expectedTask]
        1 * transaction.commit()
        1 * session.close()

        expect:
        actualTaskList.size() == 1
        actualTaskList.first().equals(expectedTask)
    }

    def "removeTask should remove Task from database"() {
        setup:
        long id = 1
        long nonExistentId = 666
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        Task existentTask = taskRepository.removeTask(id)
        Task nonExistentTask = taskRepository.removeTask(nonExistentId)

        then:
        2 * sessionFactory.openSession() >> session
        2 * session.beginTransaction() >> transaction
        1 * session.get(Task.class, id) >> expectedTask
        1 * session.get(Task.class, nonExistentId) >> null
        1 * session.delete(expectedTask)
        2 * transaction.commit()
        2 * session.close()

        expect:
        existentTask.equals(expectedTask)
        nonExistentTask == null
    }

    def "updateTask should update existing Task in database"() {
        setup:
        Task task = new Task(1, "A", "XXX", null)

        when:
        Task updatedTask = taskRepository.updateTask(task)

        then:
        1 * sessionFactory.openSession() >> session
        1 * session.beginTransaction() >> transaction
        1 * session.update(task)
        1 * transaction.commit()
        1 * session.close()

        expect:
        updatedTask.equals(task)
    }
}
