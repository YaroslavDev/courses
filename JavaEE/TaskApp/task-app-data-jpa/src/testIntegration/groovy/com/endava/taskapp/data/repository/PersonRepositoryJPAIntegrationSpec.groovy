package com.endava.taskapp.data.repository

import com.endava.taskapp.data.config.DataJPAIntegrationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = DataJPAIntegrationConfig.class)
class PersonRepositoryJPAIntegrationSpec extends PersonRepositoryIntegrationSpec {

    @Autowired
    PersonRepository personRepositoryImpl

    def setup() {
        personRepository = personRepositoryImpl
    }
}
