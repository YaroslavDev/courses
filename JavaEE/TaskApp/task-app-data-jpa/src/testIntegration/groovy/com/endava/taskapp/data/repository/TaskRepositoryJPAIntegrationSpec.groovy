package com.endava.taskapp.data.repository

import com.endava.taskapp.data.config.DataJPAIntegrationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = DataJPAIntegrationConfig.class)
class TaskRepositoryJPAIntegrationSpec extends TaskRepositoryIntegrationSpec {

    @Autowired
    TaskRepository taskRepositoryImpl

    @Autowired
    PersonRepository personRepositoryImpl

    def setup() {
        taskRepository = taskRepositoryImpl
        personRepository = personRepositoryImpl
    }
}
