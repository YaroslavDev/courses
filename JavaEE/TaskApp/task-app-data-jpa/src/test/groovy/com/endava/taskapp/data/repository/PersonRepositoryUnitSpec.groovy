package com.endava.taskapp.data.repository

import com.endava.taskapp.data.model.Person
import javax.persistence.Query
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.EntityTransaction

class PersonRepositoryUnitSpec extends Specification {
    
    PersonRepository personRepository
    EntityManagerFactory entityManagerFactory
    EntityManager entityManager
    EntityTransaction entityTransaction

    def setup() {
        entityManagerFactory = Mock()
        entityManager = Mock()
        entityTransaction = Mock()
        personRepository = new PersonRepositoryImpl(entityManagerFactory: entityManagerFactory)
    }

    def "addPerson should add new Person to database"() {
        setup:
        Person newPerson = new Person(1, "John")

        when:
        Person savedPerson = personRepository.addPerson(newPerson)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        2 * entityManager.getTransaction() >> entityTransaction
        1 * entityTransaction.begin()
        1 * entityManager.persist(newPerson)
        1 * entityTransaction.commit()
        1 * entityManager.close()

        expect:
        savedPerson.equals(newPerson)
    }

    def "getPersonById should return Person with specified id"() {
        setup:
        Person expectedPerson = new Person(1, "John")

        when:
        Person actualPerson = personRepository.getPersonById(expectedPerson.id)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        1 * entityManager.find(Person.class, expectedPerson.id) >> expectedPerson
        1 * entityManager.close()

        expect:
        actualPerson.equals(expectedPerson)
    }

    def "listPersons should return all Persons from database"() {
        setup:
        Person expectedPerson = new Person(1, "John")
        Query query = Mock()

        when:
        List<Person> actualPersonList = personRepository.listPersons()

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        1 * entityManager.createQuery("from Person") >> query
        1 * query.getResultList() >> [expectedPerson]
        1 * entityManager.close()

        expect:
        actualPersonList.size() == 1
        actualPersonList.first().equals(expectedPerson)
    }

    def "removePerson should remove Person from database"() {
        setup:
        long id = 1
        long nonExistentId = 666
        Person expectedPerson = new Person(1, "John")

        when:
        Person existentPerson = personRepository.removePerson(id)
        Person nonExistentPerson = personRepository.removePerson(nonExistentId)

        then:
        2 * entityManagerFactory.createEntityManager() >> entityManager
        4 * entityManager.getTransaction() >> entityTransaction
        2 * entityTransaction.begin()
        1 * entityManager.find(Person.class, id) >> expectedPerson
        1 * entityManager.find(Person.class, nonExistentId) >> null
        1 * entityManager.remove(expectedPerson)
        2 * entityTransaction.commit()
        2 * entityManager.close()

        expect:
        existentPerson.equals(existentPerson)
        nonExistentPerson == null
    }

    def "updatePerson should update existing Person in database"() {
        setup:
        Person person = new Person(1, "John")

        when:
        Person updatedPerson = personRepository.updatePerson(person)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        2 * entityManager.getTransaction() >> entityTransaction
        1 * entityTransaction.begin()
        1 * entityManager.merge(person)
        1 * entityTransaction.commit()
        1 * entityManager.close()

        expect:
        updatedPerson.equals(person)
    }
}
