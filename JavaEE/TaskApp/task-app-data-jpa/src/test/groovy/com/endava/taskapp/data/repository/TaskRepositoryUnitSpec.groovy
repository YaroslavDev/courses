package com.endava.taskapp.data.repository

import com.endava.taskapp.data.model.Task
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.EntityTransaction
import javax.persistence.Query


class TaskRepositoryUnitSpec extends Specification {

    TaskRepository taskRepository
    EntityManagerFactory entityManagerFactory
    EntityManager entityManager
    EntityTransaction entityTransaction

    def setup() {
        entityManagerFactory = Mock()
        entityManager = Mock()
        entityTransaction = Mock()
        taskRepository = new TaskRepositoryImpl(entityManagerFactory: entityManagerFactory)
    }

    def "addTask should add new Task to application"() {
        setup:
        Task newTask = new Task(1, "A", "XXX", null)

        when:
        Task savedTask = taskRepository.addTask(newTask)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        2 * entityManager.getTransaction() >> entityTransaction
        1 * entityTransaction.begin()
        1 * entityManager.persist(newTask)
        1 * entityTransaction.commit()
        1 * entityManager.close()

        expect:
        savedTask.equals(newTask)
    }

    def "getTaskById should return Task with specified id"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        Task actualTask = taskRepository.getTaskById(expectedTask.id)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        1 * entityManager.find(Task.class, expectedTask.id) >> expectedTask
        1 * entityManager.close()

        expect:
        actualTask.equals(expectedTask)
    }

    def "listTasks should return all Tasks from application"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)
        Query query = Mock()

        when:
        List<Task> actualTaskList = taskRepository.listTasks()

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        1 * entityManager.createQuery("from Task") >> query
        1 * query.getResultList() >> [expectedTask]
        1 * entityManager.close()

        expect:
        actualTaskList.size() == 1
        actualTaskList.first().equals(expectedTask)
    }

    def "removeTask should remove Task from database"() {
        setup:
        long id = 1
        long nonExistentId = 666
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        Task existentTask = taskRepository.removeTask(id)
        Task nonExistentTask = taskRepository.removeTask(nonExistentId)

        then:
        2 * entityManagerFactory.createEntityManager() >> entityManager
        4 * entityManager.getTransaction() >> entityTransaction
        2 * entityTransaction.begin()
        1 * entityManager.find(Task.class, id) >> expectedTask
        1 * entityManager.find(Task.class, nonExistentId) >> null
        1 * entityManager.remove(expectedTask)
        2 * entityTransaction.commit()
        2 * entityManager.close()

        expect:
        existentTask.equals(expectedTask)
        nonExistentTask == null
    }

    def "updateTask should update existing Task in database"() {
        setup:
        Task task = new Task(1, "A", "XXX", null)

        when:
        Task updatedTask = taskRepository.updateTask(task)

        then:
        1 * entityManagerFactory.createEntityManager() >> entityManager
        2 * entityManager.getTransaction() >> entityTransaction
        1 * entityTransaction.begin()
        1 * entityManager.merge(task)
        1 * entityTransaction.commit()
        1 * entityManager.close()

        expect:
        updatedTask.equals(task)
    }
}
