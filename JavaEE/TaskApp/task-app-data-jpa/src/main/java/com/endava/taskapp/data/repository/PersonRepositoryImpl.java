package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private static final Logger logger = LoggerFactory.getLogger(PersonRepositoryImpl.class);

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Person addPerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(person);
        entityManager.getTransaction().commit();

        entityManager.close();

        logger.info("Person saved succesfully, Person details = " + person);

        return person;
    }

    @Override
    public Person getPersonById(long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Person person = entityManager.find(Person.class, id);

        entityManager.close();

        logger.info("Person loaded succesfully, Person details = " + person);

        return person;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Person> listPersons() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        List<Person> personList = entityManager.createQuery("from Person").getResultList();

        entityManager.close();

        logger.info("Person list loaded succesfully: ");

        for (Person person : personList) {
            logger.info(person.toString());
        }

        return personList;
    }

    @Override
    public Person removePerson(long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Person person = entityManager.find(Person.class, id);
        entityManager.getTransaction().begin();
        try {
            entityManager.remove(person);
            logger.info("Person deleted succesfully, person details = " + person);
        } catch (IllegalArgumentException exception) {
            logger.info("Person was not found in database");
        } finally {
            entityManager.getTransaction().commit();
            entityManager.close();
        }

        return person;
    }

    @Override
    public Person updatePerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(person);
        entityManager.getTransaction().commit();

        entityManager.close();

        logger.info("Person updated succesfully, Person details = " + person);

        return person;
    }
}
