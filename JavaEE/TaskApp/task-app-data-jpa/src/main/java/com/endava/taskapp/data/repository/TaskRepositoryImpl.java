package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    private static final Logger logger = LoggerFactory.getLogger(TaskRepositoryImpl.class);

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Override
    public Task addTask(Task task) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(task);
        entityManager.getTransaction().commit();

        entityManager.close();

        logger.info("Task saved succesfully, Task details = " + task);

        return task;
    }

    @Override
    public Task getTaskById(long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Task task = entityManager.find(Task.class, id);

        entityManager.close();

        logger.info("Task loaded succesfully, Task details = " + task);

        return task;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Task> listTasks() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        List<Task> taskList = entityManager.createQuery("from Task").getResultList();

        entityManager.close();

        logger.info("Task list loaded succesfully: ");

        for (Task task : taskList) {
            logger.info(task.toString());
        }

        return taskList;
    }

    @Override
    public Task removeTask(long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Task task = entityManager.find(Task.class, id);
        entityManager.getTransaction().begin();

        try {
            entityManager.remove(task);
            logger.info("Task deleted succesfully, task details = " + task);
        } catch (IllegalArgumentException exception) {
            logger.info("Task was not found in database");
        } finally {
            entityManager.getTransaction().commit();
            entityManager.close();
        }

        return task;
    }

    @Override
    public Task updateTask(Task task) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();

        logger.info("Task updated succesfully, Task details = " + task);

        return task;
    }
}
