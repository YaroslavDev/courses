package com.endava.taskapp.data.model;

import com.endava.taskapp.data.utils.JsonUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonUnitTest {

    Person person;

    @Before
    public void preparePerson() {
        person = new Person(1, "George");
    }

    @Test
    public void testPerson() {
        Person person1 = new Person();

        person1.setId(2);
        person1.setName("Hal");

        assertTrue(person1.getId() == 2);
        assertTrue(person1.getName().equals("Hal"));
    }

    @Test
    public void testPersonWithTasks() {
        String[] titles = {"HW", "Sports"};
        String[] descriptions = {"Math, Physics, History", "Kick-boxing"};

        Task t1 = new Task(1, titles[0], descriptions[0], person);
        Task t2 = new Task(2, titles[1], descriptions[1], person);

        List<Task> tasks = new ArrayList<>();
        tasks.add(t1);
        tasks.add(t2);

        person.setTasks(tasks);

        for (int i = 0; i < person.getTasks().size(); i++) {
            assertTrue(person.getTasks().get(i).getTitle().equals(titles[i]));
            assertTrue(person.getTasks().get(i).getDescription().equals(descriptions[i]));
        }
    }

    @Test
    public void testPersonString() {
        assertFalse(person.toString().equals("Person{id=13, name=Pedro}"));

        String actualString = JsonUtils.toJsonString(person);
        assertTrue(person.toString().equals(actualString));
    }

    @Test
    public void testPersonHashCode() {
        assertFalse(person.hashCode() == 42);

        int actualHashCode = new HashCodeBuilder()
                .append(person.getId())
                .append(person.getName())
                .hashCode();
        assertTrue(person.hashCode() == actualHashCode);
    }

    @Test
    public void testPersonEquals() {
        Person person1 = new Person(1, "George");
        Person person2 = new Person(1, "Juan");
        Person person3 = new Person(2, "George");
        Task task = new Task(1, "A", "B", person);

        assertFalse(person.equals(person2));
        assertFalse(person.equals(person3));
        assertFalse(person.equals(task));
        assertFalse(person.equals(null));

        assertTrue(person.equals(person1));
        assertTrue(person.equals(person));
    }
}
