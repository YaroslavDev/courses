package com.endava.taskapp.data.model;

import com.endava.taskapp.data.utils.JsonUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TaskUnitTest {

    Task task;

    Person person;

    @Before
    public void prepareTask() {
        task = new Task(1, "Coding", "SpringMVC+Hibernate application", null);
        person = new Person(1, "Lincoln");
    }

    @Test
    public void testTask() {
        Task task1 = new Task();

        task1.setId(2);
        task1.setTitle("Reading");
        task1.setDescription("Edgar Allan Poe");
        task1.setOwner(null);

        assertTrue(task1.getId() == 2);
        assertTrue(task1.getTitle().equals("Reading"));
        assertTrue(task1.getDescription().equals("Edgar Allan Poe"));
        assertTrue(task1.getOwner() == null);
    }

    @Test
    public void testTaskWithOwner() {
        task.setOwner(person);

        assertTrue(task.getId() == 1);
        assertTrue(task.getTitle().equals("Coding"));
        assertTrue(task.getDescription().equals("SpringMVC+Hibernate application"));
        assertTrue(task.getOwner().getName().equals("Lincoln"));
    }

    @Test
    public void testTaskString() {
        assertFalse(task.toString().equals("abrakadabra"));

        String actualString = JsonUtils.toJsonString(task);
        assertTrue(task.toString().equals(actualString));
    }

    @Test
    public void testTaskHashCode() {
        assertFalse(task.hashCode() == 42);

        int actualHashCode = new HashCodeBuilder()
                .append(task.getId())
                .append(task.getTitle())
                .append(task.getDescription())
                .hashCode();
        assertTrue(task.hashCode() == actualHashCode);
    }

    @Test
    public void taskTaskEquals() {
        Task task1 = new Task(1, "Coding", "SpringMVC+Hibernate application", null);
        Task task2 = new Task(1, "Coding", "Groovy testing", null);
        Task task3 = new Task(1, "Reading", "Edgar Allan Poe", null);
        Task task4 = new Task(2, "Coding", "SpringMVC+Hibernate application", null);

        assertFalse(task.equals(task2));
        assertFalse(task.equals(task3));
        assertFalse(task.equals(task4));
        assertFalse(task.equals(person));
        assertFalse(task.equals(null));

        assertTrue(task.equals(task1));
        assertTrue(task.equals(task));
    }
}
