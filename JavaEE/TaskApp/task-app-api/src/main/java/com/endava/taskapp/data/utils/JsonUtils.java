package com.endava.taskapp.data.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class JsonUtils {

    private static final ObjectWriter WRITER = new ObjectMapper()
            .writer()
            .with(new DefaultPrettyPrinter());

    private JsonUtils() {

    }

    public static String toJsonString(final Object object) {
        try {
            return WRITER.writeValueAsString(object);
        } catch (final JsonProcessingException e) {
            return "~~";
        }
    }
}
