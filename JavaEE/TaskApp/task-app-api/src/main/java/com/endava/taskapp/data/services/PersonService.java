package com.endava.taskapp.data.services;

import com.endava.taskapp.data.model.Person;
import com.endava.taskapp.data.model.Task;

import java.util.List;

public interface PersonService {

    public Person addPerson(Person p);

    public Person getPersonById(long id);

    public List<Person> listPersons();

    public Person removePerson(long id);

    public Person updatePerson(Person p);

    public List<Task> getPersonTasks(Long id);
}
