package com.endava.taskapp.data.services;

import com.endava.taskapp.data.model.Task;

import java.util.List;

public interface TaskService {

    public Task addTask(Task p);

    public Task getTaskById(long id);

    public List<Task> listTasks();

    public Task removeTask(long id);

    public Task updateTask(Task p);
}
