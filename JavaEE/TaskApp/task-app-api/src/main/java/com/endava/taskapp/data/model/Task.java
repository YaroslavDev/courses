package com.endava.taskapp.data.model;

import javax.persistence.*;
import java.io.Serializable;

import com.endava.taskapp.data.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

    private String description;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Person owner;

    public Task() {
    }

    public Task(long id, String title, String description, Person owner) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return JsonUtils.toJsonString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(title)
                .append(description)
                .hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;

        if (getClass().equals(other.getClass())) {
            final Task otherTask = (Task) other;
            return new EqualsBuilder()
                    .append(id, otherTask.id)
                    .append(title, otherTask.title)
                    .append(description, otherTask.description)
                    .isEquals();
        }

        return false;
    }
}
