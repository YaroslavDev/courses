package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Task;

import java.util.List;

public interface TaskRepository {

    public Task addTask(Task task);

    public Task getTaskById(long id);

    public List<Task> listTasks();

    public Task removeTask(long id);

    public Task updateTask(Task task);
}
