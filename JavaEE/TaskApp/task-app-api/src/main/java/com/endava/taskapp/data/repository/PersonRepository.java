package com.endava.taskapp.data.repository;

import com.endava.taskapp.data.model.Person;

import java.util.List;

public interface PersonRepository {

    public Person addPerson(Person person);

    public Person getPersonById(long id);

    public List<Person> listPersons();

    public Person removePerson(long id);

    public Person updatePerson(Person person);
}
