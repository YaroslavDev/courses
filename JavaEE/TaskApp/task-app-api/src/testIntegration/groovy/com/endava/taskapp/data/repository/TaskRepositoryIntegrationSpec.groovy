package com.endava.taskapp.data.repository

import com.endava.taskapp.data.model.Person
import com.endava.taskapp.data.model.Task
import com.endava.taskapp.data.repository.PersonRepository
import com.endava.taskapp.data.repository.TaskRepository
import spock.lang.Specification

class TaskRepositoryIntegrationSpec extends Specification {

    TaskRepository taskRepository

    PersonRepository personRepository

    def "addTask should add new Task to application"() {
        setup:
        Person owner = new Person(name: "Yaroslav")
        personRepository.addPerson(owner)
        Task newTask = new Task(title: "A", description: "XXX", owner: owner)

        Task savedTask = taskRepository.addTask(newTask)

        expect:
        savedTask.equals(newTask)
    }

    def "getTaskById should return Task with specified id"() {
        setup:
        Person owner = new Person(1, "Yaroslav")
        Task expectedTask = new Task(1, "A", "XXX", owner)

        Task actualTask = taskRepository.getTaskById(expectedTask.id)

        expect:
        actualTask.equals(expectedTask)
    }

    def "listTasks should return all Tasks from application"() {
        setup:
        Person owner = new Person(1, "Yaroslav")
        Task expectedTask = new Task(1, "A", "XXX", owner)

        List<Person> actualTaskList = taskRepository.listTasks()

        expect:
        actualTaskList.first().equals(expectedTask)
    }

    def "updateTask should update existing Task in database"() {
        setup:
        Task task = new Task(1, "B", "YYY", null)

        Task updatedTask = taskRepository.updateTask(task)

        expect:
        updatedTask.equals(task)
    }

    def "removeTask should remove Task from database"() {
        setup:
        Task task = new Task(1, "B", "YYY", null)

        Task removedTask = taskRepository.removeTask(task.id)

        expect:
        removedTask.equals(task)
    }
}
