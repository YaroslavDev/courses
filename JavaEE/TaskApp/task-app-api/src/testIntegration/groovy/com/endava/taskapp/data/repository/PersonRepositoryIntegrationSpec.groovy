package com.endava.taskapp.data.repository

import com.endava.taskapp.data.model.Person
import com.endava.taskapp.data.repository.PersonRepository
import spock.lang.Specification

class PersonRepositoryIntegrationSpec extends Specification {

    PersonRepository personRepository

    def "addPerson should add new Person to database"() {
        setup:
        Person person = new Person(name: "Yaroslav")

        Person savedPerson = personRepository.addPerson(person)

        savedPerson.equals(person)
        savedPerson.getId() != 0
    }

    def "getPersonById should return Person with specified id"() {
        setup:
        Person expectedPerson = new Person(1, "Yaroslav")

        Person actualPerson = personRepository.getPersonById(expectedPerson.id)

        expect:
        actualPerson.equals(expectedPerson)
        actualPerson.getId() != 0
    }

    def "listPersons should return all Persons from database"() {
        setup:
        Person expectedPerson = new Person(1, "Yaroslav")

        List<Person> actualPersonList = personRepository.listPersons()

        expect:
        actualPersonList.first().equals(expectedPerson)
        actualPersonList.first().getId() != 0
    }

    def "updatePerson should update existing Person in database"() {
        setup:
        Person person = new Person(1, "Chupin Yaroslav")

        Person updatedPerson = personRepository.updatePerson(person)

        expect:
        updatedPerson.equals(person)
    }

    def "removePerson should remove Person from database"() {
        setup:
        Person person = new Person(1, "Chupin Yaroslav")

        Person removedPerson = personRepository.removePerson(person.id)

        expect:
        removedPerson.equals(person)
    }
}
