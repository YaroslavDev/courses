package com.endava.taskapp.data.services

import com.endava.taskapp.data.model.Task
import com.endava.taskapp.data.repository.TaskRepository
import spock.lang.Specification

class TaskServiceUnitSpec extends Specification {

    TaskService taskService
    TaskRepository taskRepository

    def setup() {
        taskRepository = Mock()
        taskService = new TaskServiceImpl(taskRepository: taskRepository)
    }

    def "addTask should add new Task to database"() {
        setup:
        Task newTask = new Task(1, "A", "XXX", null)

        when:
        taskService.addTask(newTask)

        then:
        1 * taskRepository.addTask(newTask)
    }

    def "getTaskById should return Task with specified id"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        Task actualTask = taskService.getTaskById(expectedTask.id)

        then:
        1 * taskRepository.getTaskById(expectedTask.id) >> expectedTask

        expect:
        actualTask.equals(expectedTask)
    }

    def "listTasks should return all Tasks from database"() {
        setup:
        Task expectedTask = new Task(1, "A", "XXX", null)

        when:
        List<Task> actualTaskList = taskService.listTasks()

        then:
        1 * taskRepository.listTasks() >> [expectedTask]

        expect:
        actualTaskList.size() == 1
        actualTaskList.first().equals(expectedTask)
    }

    def "removeTask should remove Task from database"() {
        setup:
        long id = 1

        when:
        taskService.removeTask(id)

        then:
        1 * taskRepository.removeTask(id)
    }

    def "updateTask should update existing Task in database"() {
        setup:
        Task updatedTask = new Task(1, "A", "XXX", null)

        when:
        taskService.updateTask(updatedTask)

        then:
        1 * taskRepository.updateTask(updatedTask)
    }
}
