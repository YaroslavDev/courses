package com.endava.taskapp.data.services

import com.endava.taskapp.data.model.Person
import com.endava.taskapp.data.repository.PersonRepository
import spock.lang.Specification

class PersonServiceUnitSpec extends Specification {

    PersonService personService
    PersonRepository personRepository

    def setup() {
        personRepository = Mock()
        personService = new PersonServiceImpl(personRepository: personRepository)
    }

    def "addPerson should add new Person to application"() {
        setup:
        Person newPerson = new Person(1, "John")

        when:
        personService.addPerson(newPerson)

        then:
        1 * personRepository.addPerson(newPerson)
    }

    def "getPersonById should return Person with specified id"() {
        setup:
        Person expectedPerson = new Person(1, "John")

        when:
        Person actualPerson = personService.getPersonById(expectedPerson.id)

        then:
        1 * personRepository.getPersonById(expectedPerson.id) >> expectedPerson

        expect:
        actualPerson.equals(expectedPerson)
    }

    def "listPersons should return all Persons from application"() {
        setup:
        Person expectedPerson = new Person(1, "John")

        when:
        List<Person> actualPersonList = personService.listPersons()

        then:
        1 * personRepository.listPersons() >> [expectedPerson]

        expect:
        actualPersonList.size() == 1
        actualPersonList.first().equals(expectedPerson)
    }

    def "removePerson should remove Person from application"() {
        setup:
        long id = 1

        when:
        personService.removePerson(id)

        then:
        1 * personRepository.removePerson(id)
    }

    def "updatePerson should update existing Person in application"() {
        setup:
        Person updatedPerson = new Person(1, "John")

        when:
        personService.updatePerson(updatedPerson)

        then:
        1 * personRepository.updatePerson(updatedPerson)
    }
}
