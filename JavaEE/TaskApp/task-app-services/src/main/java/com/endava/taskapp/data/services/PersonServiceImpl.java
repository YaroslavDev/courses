package com.endava.taskapp.data.services;

import com.endava.taskapp.data.model.Person;
import com.endava.taskapp.data.model.Task;
import com.endava.taskapp.data.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    @Transactional
    public Person addPerson(Person person) {
        return personRepository.addPerson(person);
    }

    @Override
    @Transactional
    public Person getPersonById(long id) {
        return personRepository.getPersonById(id);
    }

    @Override
    @Transactional
    public List<Task> getPersonTasks(Long id) {
        Person person = personRepository.getPersonById(id);
        List<Task> tasks = person.getTasks();
        return tasks;
    }

    @Override
    @Transactional
    public List<Person> listPersons() {
        return personRepository.listPersons();
    }

    @Override
    @Transactional
    public Person removePerson(long id) {
        return personRepository.removePerson(id);
    }

    @Override
    @Transactional
    public Person updatePerson(Person person) {
        return personRepository.updatePerson(person);
    }
}
