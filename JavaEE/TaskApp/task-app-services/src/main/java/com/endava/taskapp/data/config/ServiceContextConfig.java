package com.endava.taskapp.data.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.endava.taskapp.data.services"})
public class ServiceContextConfig {
}
