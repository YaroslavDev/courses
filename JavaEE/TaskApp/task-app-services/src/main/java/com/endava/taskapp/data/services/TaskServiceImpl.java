package com.endava.taskapp.data.services;

import com.endava.taskapp.data.model.Task;
import com.endava.taskapp.data.repository.PersonRepository;
import com.endava.taskapp.data.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PersonRepository personRepository;

    @Override
    @Transactional
    public Task addTask(Task task) {
        return taskRepository.addTask(task);
    }

    @Override
    @Transactional
    public Task getTaskById(long id) {
        return taskRepository.getTaskById(id);
    }

    @Override
    @Transactional
    public List<Task> listTasks() {
        return taskRepository.listTasks();
    }

    @Override
    @Transactional
    public Task removeTask(long id) {
        return taskRepository.removeTask(id);
    }

    @Override
    @Transactional
    public Task updateTask(Task task) {
        return taskRepository.updateTask(task);
    }
}
