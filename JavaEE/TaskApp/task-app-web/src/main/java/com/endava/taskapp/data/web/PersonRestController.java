package com.endava.taskapp.data.web;

import com.endava.taskapp.data.model.Person;
import com.endava.taskapp.data.model.Task;
import com.endava.taskapp.data.services.PersonService;
import com.endava.taskapp.data.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class PersonRestController {

    @Autowired
    private PersonService personService;

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public List<Person> listPersons() {
        List<Person> personList = personService.listPersons();
        return personList;
    }

    @RequestMapping(value = "/persons", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Person addPerson(@RequestBody Person person) {
        return personService.addPerson(person);
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.GET)
    public Person getPerson(@PathVariable("id") Long id) {
        return personService.getPersonById(id);
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Person updatePerson(@PathVariable("id") Long id, @RequestBody Person person) {
        person.setId(id);
        return personService.addPerson(person);
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.DELETE)
    public Person removePerson(@PathVariable("id") Long id) {
        return personService.removePerson(id);
    }

    @RequestMapping(value = "/persons/{id}/tasks", method = RequestMethod.GET)
    public List<Task> getPersonTasks(@PathVariable("id") Long id) {
        List<Task> tasks = personService.getPersonTasks(id);
        return tasks;
    }

    @RequestMapping(value = "/persons/{id}/tasks", method = RequestMethod.POST)
    public Task addPersonTask(@PathVariable("id") Long id, @RequestBody Task task) {
        Person owner = personService.getPersonById(id);
        task.setOwner(owner);
        return taskService.addTask(task);
    }

    @RequestMapping(value = "/persons/{personId}/tasks/{taskId}", method = RequestMethod.PUT)
    public Task addPersonTask(@PathVariable("personId") Long pid,
                              @PathVariable("taskId") Long tid,
                              @RequestBody Task task) {
        task.setId(tid);
        Person owner = personService.getPersonById(pid);
        task.setOwner(owner);
        return taskService.updateTask(task);
    }

    @RequestMapping(value = "/persons/{pid}/tasks/{tid}", method = RequestMethod.DELETE)
    public Task removePerson(@PathVariable("pid") Long pid,
                             @PathVariable("tid") Long tid) {
        return taskService.removeTask(tid);
    }
}
