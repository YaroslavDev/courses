package com.endava.taskapp.data.web;

import com.endava.taskapp.data.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PersonWebController {

    @Autowired
    PersonRepository personRepository;

    @RequestMapping(value = {"/persons" , "/"}, method = RequestMethod.GET)
    public String listPersons(Model model) {

        return "persons";
    }
}
