(function() {
    var app = angular.module('taskapp', ['taskapp-directives']);

    app.controller('PersonController', [ '$http', function($http) {
        var ctrl = this;

        ctrl.persons = [ ];

        $http.get('/api/v1/persons').success(function(persons) {
            ctrl.persons = persons;

            ctrl.persons.forEach(function(pers) {
               $http.get('/api/v1/persons/' + pers.id + '/tasks').success(function(tasks) {
                   pers.tasks = tasks;
               });
            });

        });
    }]);
})();