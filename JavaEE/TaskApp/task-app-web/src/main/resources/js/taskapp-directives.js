(function() {
    var app = angular.module('taskapp-directives', ['ngRoute']);

    app.directive('loginForm', function() {
       return {
           restrict: 'E',
           templateUrl: 'webjars/templates/login-form.html'
       };
    });

    app.directive('personName', function() {
       return {
           restrict: 'E',
           templateUrl: 'webjars/templates/person-name.html'
       };
    });

    app.directive('personTasks', function() {
       return {
           restrict: 'E',
           templateUrl: 'webjars/templates/person-tasks.html',
           controller: [ '$http', '$route', function($http, $route) {
               this.newTask = {};

               var newTask = this.newTask;
               this.addNewTask = function(person) {
                   this.newTask.owner = person;
                   $http.post('api/v1/persons/' + person.id + '/tasks', this.newTask).success(function(savedTask) {
                       person.tasks.push(savedTask);
                       newTask = {};
                   });
               };

               this.saveTask = function(person, task) {
                   $http.put('api/v1/persons/' + person.id + '/tasks/' + task.id, task).success(function(updatedTask) {
                       var id = person.tasks.indexOf(task);
                       if (id > -1) {
                           person.tasks[id] = updatedTask;
                       }
                   });
               };

               this.deleteTask = function(person, task) {
                   $http.delete('api/v1/persons/' + person.id + '/tasks/' + task.id).success(function(data) {
                       var id = person.tasks.indexOf(task);
                       if (id > -1) {
                           person.tasks.splice(id, 1);
                       }
                   });
               }
           }],
           controllerAs: 'taskCtrl'
       };
    });
})();