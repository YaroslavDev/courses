1.	Create an array which consists of 5 strings, print the content by iterating through the array.
2.	Given a word represented as array of chars. Check if the char sequence is a palindrome:
a.	Simple case for any character: string �acca� is a palindrome, �acc,a� is not
b.	2nd case check only for alphanumeric chars: both �acca� and �acc,a� are palindromes
3.	Create an array of numbers (int, double etc. of your choice). Write a method which sorts that array in ascending mode using a sorting algorithm (of your choice). 
4.	Create an array of numbers (int, double etc. of your choice). Write a method which finds the maximum element.
5.	Read from the keybord a number N. Based on this number, create the following output (in this particular case N = 5):
N	10*N	100*N
1	10		100
2	20		200
3	30		300
4	40		400
5	50		500
6.	Create a class SavingsAccount (for keeping deposits). Use a static variable annualInterestRate for keeping information about the interest rate, which is common for all depositors. Every instance of this class has a private member savingsBalance, which represent the amount that depositor has on account. Write a method calculateMonthlyInterest that determines monthly benefit using the following formula: (savingsBalance* annualInterestRate)/12; the result should be added to savingsBalance. Write a static method modifyInterestRate that gives a new value to annualInterestRate. Write a program that tests SavingsAccount. Create 2 different objects of type SavingsAccount � saver1 and saver2, with balance $2000.00 and $3000.00. Set annualInterestRate equal to 4%, then calculate monthly benefit and add it savingsBalance. Display new values for deposits. Modify annualInterestRate in order to be equal to 5%. Calculate monthly benefit and add it savingsBalance. Display new values for deposits.
