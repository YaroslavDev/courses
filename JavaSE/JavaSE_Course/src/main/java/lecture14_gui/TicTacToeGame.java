package lecture14_gui;

import java.util.Random;

/**
 * Implementation of a GridGame as classical TicTacToe game
 *
 */
public class TicTacToeGame extends GridGame {

    /**
     * Indicates whose playes is turn to select cell
     */
    protected boolean playerOneTurn = true;
    /**
     * Game state:
     * <ul>
     *     <li>-10 - circle win</li>
     *     <li>-1 - circle's turn</li>
     *     <li>0 - game in progress</li>
     *     <li>1 - cross's turn</li>
     *     <li>10 - cross win</li>
     *     <li>20 - draw</li>
     * </ul>
     */
    protected int state = 0;

    /**
     * Getter for state
     *
     * @return returns game's current state
     */
    public int getState() {
        return state;
    }

    /**
     * Setter for state
     *
     * @param state set current game state
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * Constructor takes grid size as argument
     *
     * @param s grid size
     */
    public TicTacToeGame(int s) {
        super(s);
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = 0;
            }
        }
        Random r = new Random();
        playerOneTurn = r.nextBoolean();
    }

    /**
     * In dependence on whose turn selects cell and marks it with corresponding value
     *
     * @param i vertical index of cell
     * @param j horizontal index of cell
     * @return (i, j) cell value
     */
    @Override
    public int selectCell(int i, int j) {
        grid[i][j] = playerOneTurn ? 1 : -1;
        playerOneTurn = !playerOneTurn;
        state = checkState();
        return grid[i][j];
    }

    /**
     * Method that scans grid and detects games current state
     *
     * @return returns state
     */
    public int checkState() {
        // Horizontal & Vertical
        int horizontalSum = 0;
        int verticalSum = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                horizontalSum += grid[i][j];
                verticalSum += grid[j][i];
            }
            if (horizontalSum == grid.length || verticalSum == grid.length) {
                return 10;
            }
            if (horizontalSum == -grid.length || verticalSum == -grid.length) {
                return -10;
            }
            horizontalSum = 0;
            verticalSum = 0;
        }
        // Diagonals
        int mainSum = 0, secondarySum = 0;
        for (int i = 0; i < grid.length; i++) {
            mainSum += grid[i][i];
            secondarySum += grid[i][grid.length - i - 1];
        }
        if (mainSum == grid.length || secondarySum == grid.length) {
            return 10;
        }
        if (mainSum == -grid.length || secondarySum == -grid.length) {
            return -10;
        }
        // Check if non-empty
        boolean nonEmpty = true;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                if (grid[i][j] == 0) return 0;
            }
        }
        return 20;
    }
}
