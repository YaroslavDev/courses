package lecture14_gui;

/**
 * Encapsulates basic functionality of an abstract grid game
 */
public class GridGame {

    /**
     * Stores grid itself as 2-dimensional NxN array of ints
     */
    protected int[][] grid;

    /**
     * Constructor that takes grid size as argument
     *
     * @param s grid size
     */
    public GridGame(int s) {
        grid = new int[s][s];
    }

    /**
     * Select cell located at (i, j) and return its value
     *
     * @param i vertical index of cell in array
     * @param j horizontal index of cell in array
     * @return returns values of selected cell
     */
    public int selectCell(int i, int j) {
        return 0;
    }

    /**
     * Get game grid size
     *
     * @return
     */
    public int getSize() {
        return grid.length;
    }
}
