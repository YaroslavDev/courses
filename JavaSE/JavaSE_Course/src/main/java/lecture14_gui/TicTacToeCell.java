package lecture14_gui;

import javax.swing.*;
import java.awt.*;

/**
 * Implements custom JButton that renders cross or circle on itself.
 *
 * @See <a href="https://docs.oracle.com/javase/7/docs/api/javax/swing/JButton.html">JButton</a>
 */
public class TicTacToeCell extends JButton {

    /**
     * Buttons x and y coordinates in pixels
     */
    private int x, y;
    /**
     * Symbol that button has assigned
     *
     * <ul>
     *     <li>1 - cross</li>
     *     <li>-1 - circle</li>
     *     <li>otherwise - empty</li>
     * </ul>
     */
    private int symbol = 0;

    /**
     *
     * @param _x left upper corner x coordinate in pixels
     * @param _y left upper corner y coordinate in pixels
     */
    public TicTacToeCell(int _x, int _y) {
        super();
        x = _x;
        y = _y;
        enableInputMethods(true);
    }

    /**
     * Assigns symbol to button
     *
     * @param s new symbol
     */
    public void selectCell(int s) {
        symbol = s;
    }

    /**
     * Method that does painting itself
     *
     * @param g Graphics using which drawing is performed
     * @See Graphics
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int width = getSize().width;
        int height = getSize().height;
        g.setColor(Color.black);
        g.drawRect(x, y, width, height);
        if (symbol == 1) {
            g.drawLine(x, y, x + width, y + height);
            g.drawLine(x + width, y, x, y + width);
        } else if (symbol == -1) {
            g.drawOval(x, y, width, height);
        }
    }
}
