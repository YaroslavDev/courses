package lecture14_gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Implements custom JFrame that renders TicTacToe grid with crosses and circles
 *
 * @See <a href="http://docs.oracle.com/javase/7/docs/api/javax/swing/JFrame.html">JFrame</a>
 */
public class GridGameFrame extends JFrame {

    /**
     * java.awt.event.ActionListener listens to user actions(ex. clicked())
     *
     * @See ActionListener
     */
    protected ActionListener listener;
    /**
     * 2-dimensional array of TicTacToeCell
     */
    protected TicTacToeCell[][] buttonGrid;
    /**
     * Cell size in pixels
     */
    protected int cellSize;

    /**
     *
     * @param name window title name
     * @param s grid size
     * @param c cell size
     */
    public GridGameFrame(String name, int s, int c) {
        super(name);
        cellSize = c;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel parent = new JPanel();
        parent.setLayout(new GridLayout(s, s));
        buttonGrid = new TicTacToeCell[s][s];
        for (int i = 0; i < buttonGrid.length; i++) {
            for (int j = 0; j < buttonGrid[i].length; j++) {
                buttonGrid[i][j] = new TicTacToeCell(i, j);
                buttonGrid[i][j].setSize(c, c);
                buttonGrid[i][j].setActionCommand("" + i + "." + j);
                parent.add(buttonGrid[i][j]);
            }
        }
        Dimension dims = new Dimension(buttonGrid.length * cellSize, buttonGrid.length * cellSize);
        getContentPane().setPreferredSize(dims);
        parent.setPreferredSize(dims);

        add(parent);
        setResizable(false);
    }

    /**
     * Getter for listener
     *
     * @return listener
     */
    public ActionListener getListener() {
        return listener;
    }

    /**
     * Setter for listener
     *
     * @param listener new listener
     */
    public void setListener(ActionListener listener) {
        this.listener = listener;
        for (int i = 0; i < buttonGrid.length; i++) {
            for (int j = 0; j < buttonGrid[i].length; j++) {
                buttonGrid[i][j].addActionListener(this.listener);
            }
        }
    }

    /**
     * Informs corresponding cell that it was touched and it should re-render itself with specified symbol
     *
     * @param i  vertical index of cell in array
     * @param j  horizontal index of cell in array
     * @param symbol symbol that should be assigned to selected cell
     */
    public void selectCell(int i, int j, int symbol) {
        buttonGrid[i][j].selectCell(symbol);
    }
}
