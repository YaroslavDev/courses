package lecture14_gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Implements custom ActionListener that reacts to user's clicks and
 * manages underlying view GridGameFrame and model TicTacToeGame
 *
 * @See <a href="https://docs.oracle.com/javase/7/docs/api/java/awt/event/ActionListener.html">ActionListener</a>
 */
public class TicTacToeGameController implements ActionListener {

    /**
     * Game's model, business logic
     */
    private TicTacToeGame game;
    /**
     * Game's view, graphical part
     */
    private GridGameFrame frame;

    public TicTacToeGameController() {
    }

    /**
     * Restarts game at the beginning and after game is finished
     */
    public void restart() {
        game = null;
        if (frame != null) {
            frame.setVisible(false);
            frame.dispose();
        }
        game = new TicTacToeGame(3);
        frame = new GridGameFrame("TicTacToeGame", game.getSize(), 100);
        frame.setListener(this);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Callback that gets called every time user selects cell
     *
     * @param e event object containing info about selected cell
     */
    public void actionPerformed(ActionEvent e) {
        String[] indices = e.getActionCommand().split("\\.");
        int i = Integer.parseInt(indices[0]);
        int j = Integer.parseInt(indices[1]);
        frame.selectCell(i, j, game.selectCell(i, j));
        if (game.getState() != 0) {
            switch (game.getState()) {
                case 10:
                    JOptionPane.showMessageDialog(frame, "Cross won!");
                    break;
                case -10:
                    JOptionPane.showMessageDialog(frame, "Circle won!");
                    break;
                case 20:
                    JOptionPane.showMessageDialog(frame, "Draw!");
                default:
                    break;
            }
            restart();
        }
    }

    /**
     * Main static method that runs game
     *
     * @param args command-line args
     */
    public static void main(String[] args) {
        TicTacToeGameController ctrl = new TicTacToeGameController();
        ctrl.restart();
    }
}
