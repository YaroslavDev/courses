package lecture9_threads;

public class Producer extends Thread {
    private Stock stock;
    private int number;

    public Producer(Stock c, int number) {
        stock = c;
        this.number = number;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            stock.put(i);
            System.out.println("Producer #" + this.number
                    + " put: " + i);
            try {
                sleep((int)(Math.random() * 100));
            } catch (InterruptedException e) { }
        }
    }
}
