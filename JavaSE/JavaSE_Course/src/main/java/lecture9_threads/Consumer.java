package lecture9_threads;

public class Consumer extends Thread {
    private Stock stock;
    private int number;

    public Consumer(Stock c, int number) {
        stock = c;
        this.number = number;
    }

    public void run() {
        int value = 0;
        for (int i = 0; i < 10; i++) {
            value = stock.get();
            System.out.println("Consumer #" + this.number
                    + " got: " + value);
        }
    }
}
