package lecture1_oop;

public class Manager extends Employee {
	public static double BONUS = 0.5;	// 50%
	
	public double getSalary() {
		return (1.0 + Manager.BONUS) * super.getSalary();
	}
	
	public void displayInfo() {
		System.out.println("Manager's tariff per hour = " + getTariffPerHour());
		System.out.println("Manager's salary = " + getSalary());
	}
}
