package lecture1_oop;

public class Point {
	private int x, y, z;
	
	public Point(int _x, int _y, int _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	
	public Point(int _x, int _y) {
		x = _x;
		y = _y;
	}

	public int getX() {
		return x;
	}

	public Point setX(int x) {
		this.x = x;
		return this;
	}

	public int getY() {
		return y;
	}

	public Point setY(int y) {
		this.y = y;
		return this;
	}

	public int getZ() {
		return z;
	}

	public Point setZ(int z) {
		this.z = z;
		return this;
	}
	
	public String toString() {
		return "(" + x + ", " + y + ", " + z + ")";
	}
}
