package lecture1_oop;

public class Square extends GraphicObject {
	private double sideLength;
	
	public Square(double side) {
		sideLength = side;
	}
	
	public double getArea() {
		return sideLength * sideLength;
	}
	
	public double getPerimeter() {
		return 4 * sideLength;
	}
	
	public void displayInfo() {
		System.out.println("Square has area " + getArea());
		System.out.println("Square has perimeter " + getPerimeter());
	}
}
