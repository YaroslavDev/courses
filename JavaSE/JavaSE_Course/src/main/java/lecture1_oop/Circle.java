package lecture1_oop; 

public class Circle extends GraphicObject {
	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}
	
	public void displayInfo() {
		System.out.println("Circle has area " + getArea());
		System.out.println("Circle has perimeter " + getPerimeter());
	}
}
