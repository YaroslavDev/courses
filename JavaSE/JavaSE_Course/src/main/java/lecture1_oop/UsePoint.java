package lecture1_oop;

public class UsePoint {
	public static void main(String[] args) {
		Point p = new Point(0, 0, 0);
		p.setX(4).setY(5).setZ(-6);
		System.out.println("P is " + p);
		
		Point p1 = new Point(4, 1);
		System.out.println("P1 is " + p1);
	}
}
