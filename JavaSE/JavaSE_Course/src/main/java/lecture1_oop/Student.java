package lecture1_oop;

public class Student {
	private String name;
	private int age;
	
	public void setData (String n, int a) {
		name = n;
		age = a;
	}
	
	public int getAge() {
		return age;
	}
	
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.setData("John", 23);
		Student s2 = new Student();
		s2.setData("Alex", 28);
		System.out.println("Average age is " + (s1.getAge() + s2.getAge()) / 2.0);
	}
}
