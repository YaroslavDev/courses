package lecture1_oop;

public class UseEmployee {
	public static void main(String[] args) {
		Employee e = new Employee();
		e.hours = 160;	//month of 5 days per week, 8 hour per day work
		e.displayInfo();
		
		Employee e1 = new Manager();
		e1.hours = 160;
		e1.displayInfo();
	}
}
