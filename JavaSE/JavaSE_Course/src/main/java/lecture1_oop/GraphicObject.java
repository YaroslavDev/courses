package lecture1_oop;

public abstract class GraphicObject {
    public abstract double getArea();
    public abstract double getPerimeter();
    public void displayInfo() {
    }
}
