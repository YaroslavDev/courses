package lecture1_oop;

public class Employee {
	public int hours;
	private double tariffPerHour = 5.5;
	
	public double getSalary() {
		return tariffPerHour * hours;
	}

	public double getTariffPerHour() {
		return tariffPerHour;
	}
	
	public void displayInfo() {
		System.out.println("Employee's tariff per hour = " + getTariffPerHour());
		System.out.println("Employee's salary = " + getSalary());
	}
}
