package lecture13_mocking;

public interface NumberProvider {

    public int getNumber();
}
