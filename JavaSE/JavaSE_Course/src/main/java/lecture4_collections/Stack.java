package lecture4_collections;

import java.util.NoSuchElementException;

public class Stack<T> {
    protected StackElement head;

    public class StackElement {
        public T value;
        public StackElement prev;

        public StackElement(T v, StackElement p) {
            value = v;
            prev = p;
        }
    }

    public Stack() {
        head = null;
    }

    public void push(T e) {
        if (head == null) {
            head = new StackElement(e, null);
        } else {
            StackElement se = new StackElement(e, head);
            head = se;
        }
    }

    public T pop() {
        T v = null;
        if (head != null) {
            v = head.value;
            head = head.prev;
        } else {
            throw new NoSuchElementException("Stack is empty");
        }
        return v;
    }
}
