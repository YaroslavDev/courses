package lecture4_collections;

public class List<T> {
    protected ListElement head, tail;
    private int size;

    public class ListElement {
        public T value;
        public ListElement next;

        public ListElement(T v, ListElement n) {
            value = v;
            next = n;
        }
    }

    public List() {
        head = null;
        tail = null;
        size = 0;
    }

    public void add(T v) {
        ListElement le = new ListElement(v, null);
        if (head == null && tail == null) {
            tail = le;
            head = tail;
        } else {
            tail.next = le;
            tail = le;
        }
        size++;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Out of List bounds");
        }
        ListElement le = head;
        T val = null;
        if (index == 0) {
            val = head.value;
            head = head.next;
        } else {
            for (int i = 0; i < index - 1; i++) {
                le = le.next;
            }
            val = le.next.value;
            le.next = le.next.next;
            if (index == size - 1) {
                tail = le;
            }
        }
        size--;
        return val;
    }
}
