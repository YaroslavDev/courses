package lecture2_classes;

public class ArrayTask {
	public static void main(String[] args) {
		System.out.println("Task #1: ");
		String[] strArray = new String[] { "Java", "C++", "Python", "Scala", "Javascript" };
		for (String s : strArray) {
			System.out.println(s);
		}
		System.out.println("========================");
	}
}
