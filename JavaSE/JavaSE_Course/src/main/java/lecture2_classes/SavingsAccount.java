package lecture2_classes;

public class SavingsAccount {
    static double annualInterestRate = 0.05;

    private double savingsBalance;

    public double getSavingsBalance() {
        return savingsBalance;
    }

    public void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    public SavingsAccount(double balance) {
        savingsBalance = balance;

    }

    public double calculateMonthlyInterest() {
        double interest = savingsBalance * annualInterestRate / 12;

        savingsBalance += interest;

        return interest;
    }

    public static void modifyInterestRate(double rate) {
        annualInterestRate = rate;
    }
}
