package lecture2_classes;

import java.util.Scanner;

public class NTask {
	public static void main(String[] args) {
		System.out.println("Task #5: ");
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		in.close();
		System.out.printf("%-5s %-5s %-5s\n", "N", "10*N", "100*N");
		for (int i = 1; i <= N; i++) {
            System.out.printf("%-5d %-5d %-5d\n", i, i * 10, i * 100);
			//System.out.println("" + i + "\t" + i * 10 + "\t" + i * 100);
		}
		System.out.println("========================");
	}
}
