package lecture2_classes;

public class PalindromeTask {
	public static boolean isPalindrome(char[] s, boolean alphanumeric) {
		if (alphanumeric) {
			int alphaNumericCount = 0;
			for (char c : s) {
				if (Character.isDigit(c) || Character.isAlphabetic(c)) {
					alphaNumericCount++;
				}
			}
			char[] filtered = new char[alphaNumericCount];
			int i = 0;
			for (char c : s) {
				if (Character.isDigit(c) || Character.isAlphabetic(c)) {
					filtered[i++] = c; 
				}
			}
			s = filtered;
		} 
		int n = s.length;
		for (int i = 0; i < n / 2; i++) {
			if (Character.toLowerCase(s[i]) != Character.toLowerCase(s[n - i - 1])) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Task #2: ");
		String[] tests = { "A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!", 
				"acca", "acc,a", 
				"A car, a man, a maraca.", 
				""};
		for (String test : tests) {
			System.out.println("palindrome(" + test + ") = " + isPalindrome(test.toCharArray(), false));
			System.out.println("alpha-palindrome(" + test + ") = " + isPalindrome(test.toCharArray(), true));
		}
		System.out.println("========================");
	}
}
