package lecture2_classes;

public class MaxTask {
	public static int max(int[] arr) {
		int max = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (max < arr[i]) {
				max = arr[i];
			}
		}
		return max;
	}
	public static void main(String[] args) {
		System.out.println("Task #4: ");
		int[] arr = new int[] { 7, 4, 5, 9, 1, -3, 0, 10, 6 };
		SortTask.printArray(arr);
		System.out.println("Max is " + max(arr));
		System.out.println("========================");
	}
}
