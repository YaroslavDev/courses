package lecture2_classes;

import java.util.ArrayList;
import java.util.List;

public class SortTask {
	public static void swap(int[] arr, int l, int r) {
		int buff = arr[l];
		arr[l] = arr[r];
		arr[r] = buff;
	}
	
	public static void quickSort(int[] arr, int low, int high) {		
		int l = low;
		int r = high;
		int pivot = arr[(l + r) / 2];
		
		while (l <= r) {
			while (arr[l] < pivot) l++;
			while (arr[r] > pivot) r--;
		
			if (l <= r) {
				swap(arr, l, r);
				l++;
				r--;
			}
		}
		
		if (low < r) quickSort(arr, low, r);
		if (l < high) quickSort(arr, l, high);
	}
	
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) System.out.print(" " + arr[i]);
		System.out.println();
	}
	
	public static void main(String[] args) {
		List<Integer> l = new ArrayList<Integer>() {{ add(1); add(2); }};

		System.out.println("Task #3: ");
		int[] arr = new int[] { 7, 4, 5, 9, 1, -3, 0, 10, 6 };
		System.out.println("Unsorted: ");
		printArray(arr);
		
		quickSort(arr, 0, arr.length - 1);
		
		System.out.println("Sorted: ");
		printArray(arr);
		System.out.println("========================");
	}
}
