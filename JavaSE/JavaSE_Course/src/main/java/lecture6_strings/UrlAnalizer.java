package lecture6_strings;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlAnalizer {
    public static ArrayList<String> getHTML(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        ArrayList<String> result = new ArrayList<String>();
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result.add(line);
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        String source = "";
        String pattern = "";
        if (args.length >= 2) {
            source = args[0];
            pattern = args[1];
        } else {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter source to scan:");
            source = in.nextLine();
            System.out.println("Enter pattern to match:");
            pattern = in.nextLine();
        }

        System.out.println("Fetching started...");
        ArrayList<String> html = getHTML(source);
        System.out.println("Fetched: ");
        for (int i = 0; i < html.size(); i++) {
            System.out.println(i + " " + html.get(i));
        }

        System.out.println("Scanning started...");
        Pattern re = Pattern.compile(pattern);
        for (int i = 0; i < html.size(); i++) {
            Matcher matcher = re.matcher(html.get(i));
            while (matcher.find()) {
                String out = String.format("Line %d: from %d to %d", i, matcher.start(), matcher.end());
                System.out.println(out);
            }
        }
    }
}
