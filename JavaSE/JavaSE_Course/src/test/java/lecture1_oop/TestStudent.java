package lecture1_oop;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestStudent {

    @Test
    public void testStudent() {
        Student s = new Student();
        s.setData("John", 20);

        assertTrue(s.getAge() == 20);
    }
}
