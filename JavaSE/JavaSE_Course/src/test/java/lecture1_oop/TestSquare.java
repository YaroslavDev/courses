package lecture1_oop;

import org.junit.Test;
import static org.junit.Assert.*;


public class TestSquare {

    @Test
    public void testSquareArea() {
        Square s = new Square(5.0);

        assertTrue(Math.abs(s.getArea() - 25.0) < 0.001);
    }

    @Test
    public void testSquarePerimeter() {
        Square s = new Square(5.0);

        assertTrue(Math.abs(s.getPerimeter() - 20.0) < 0.001);
    }
}
