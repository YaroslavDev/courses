package lecture1_oop;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestPoint {

    @Test
    public void testPoint() {
        Point p = new Point(4, 2, 1);
        assertTrue(p.getX() == 4 && p.getY() == 2 && p.getZ() == 1);

        p.setX(-1).setY(9).setZ(0);
        assertTrue(p.getX() == -1 && p.getY() == 9 && p.getZ() == 0);
    }
}
