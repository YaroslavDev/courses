package lecture1_oop;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestEmployee {

    @Test
    public void testEmployeeTariff() {
        Employee e = new Employee();

        assertTrue(e.getTariffPerHour() == 5.5);
    }

    @Test
    public void testEmployeeSalary() {
        Employee e = new Employee();
        e.hours = 160;

        assertTrue(e.getSalary() == 880.0);
    }
}
