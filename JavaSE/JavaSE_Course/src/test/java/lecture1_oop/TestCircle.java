package lecture1_oop;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestCircle {

    @Test
    public void testCircleArea() {
        Circle c = new Circle(3);

        assertTrue(Math.abs(c.getArea() - 28.274) < 0.001);
    }

    @Test
    public void testCirclePerimeter() {
        Circle c = new Circle(3);

        assertTrue(Math.abs(c.getPerimeter() - 18.849) < 0.001);
    }
}
