package lecture1_oop;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestManager {
    @Test
    public void testManagerTariff() {
        Employee e = new Manager();

        assertTrue(e.getTariffPerHour() == 5.5);
    }

    @Test
    public void testManagerSalary() {
        Employee e = new Manager();
        e.hours = 160;

        assertTrue(e.getSalary() == 1320.0);
    }
}
