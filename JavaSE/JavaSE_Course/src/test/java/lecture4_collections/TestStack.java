package lecture4_collections;

import org.junit.Test;
import java.util.NoSuchElementException;
import static org.junit.Assert.assertTrue;

public class TestStack {

    @Test(expected = NoSuchElementException.class)
    public void testEmptyStack() {
        Stack<String> s = new Stack<String>();
        s.pop();
    }

    @Test(expected = NoSuchElementException.class)
    public void testStackException() {
        Stack<String> s = new Stack<String>();
        s.push("Java");
        s.pop();
        s.pop();
    }


    @Test
    public void testSingleElementStack() {
        Stack<String> s = new Stack<String>();
        s.push("Java");
        assertTrue(s.pop().equals("Java"));
    }

    @Test
    public void testMultipleElementStack() {
        Stack<String> s = new Stack<String>();
        s.push("Java");
        s.push("C++");
        s.push("C#");
        assertTrue(s.pop().equals("C#"));
        assertTrue(s.pop().equals("C++"));
        s.push("Python");
        s.push("Ruby");
        assertTrue(s.pop().equals("Ruby"));
        assertTrue(s.pop().equals("Python"));
        assertTrue(s.pop().equals("Java"));
    }
}
