package lecture4_collections;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestList {

    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyList() {
        List<String> l = new List<String>();
        l.get(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListException() {
        List<String> l = new List<String>();
        l.add("Spring");
        l.get(0);
        l.get(0);
    }

    @Test
    public void testSingleElementList() {
        List<String> l = new List<String>();
        l.add("Spring");
        assertTrue(l.get(0).equals("Spring"));
    }

    @Test
    public void testMultipleElementList() {
        List<String> l = new List<String>();
        l.add("Spring");
        l.add("Hybernate");
        l.add("Play!");
        l.add("JSF");
        assertTrue(l.get(1).equals("Hybernate"));
        assertTrue(l.get(2).equals("JSF"));
        assertTrue(l.get(1).equals("Play!"));
        l.add("Wicket");
        l.add("Vaadin");
        assertTrue(l.get(0).equals("Spring"));
        assertTrue(l.get(1).equals("Vaadin"));
        assertTrue(l.get(0).equals("Wicket"));
    }
}
