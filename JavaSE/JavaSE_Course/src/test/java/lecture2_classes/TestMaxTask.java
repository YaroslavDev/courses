package lecture2_classes;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestMaxTask {
    @Test
    public void testMax() {
        int[] arr = new int[] {10, 9, 1, 20, -3, 0, 40, 12};

        assertTrue(MaxTask.max(arr) == 40);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testMaxThrowException() {
        int[] arr = new int[] {};

        MaxTask.max(arr);
    }
}
