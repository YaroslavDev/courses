package lecture2_classes;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPalindromeTask {

    @Test
    public void testPalindromNonalphanumeric() {
        String s1 = "Hannah";
        assertTrue(PalindromeTask.isPalindrome(s1.toCharArray(), false));

        String s2 = "A dog, a plan, a canal: pagoda.";
        assertFalse(PalindromeTask.isPalindrome(s2.toCharArray(), false));
    }

    @Test
    public void testPalindromAlphanumeric() {
        String s1 = "alula";
        assertTrue(PalindromeTask.isPalindrome(s1.toCharArray(), true));

        String s2 = "I made border bard’s drowsy swords; drab, red robed am I.";
        assertTrue(PalindromeTask.isPalindrome(s2.toCharArray(), true));
    }
}
