package lecture2_classes;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestSavingsAccount {
    @Test
    public void testSavingsAccount() {
        SavingsAccount saver1 = new SavingsAccount(2000),
                saver2 = new SavingsAccount(3000);

        SavingsAccount.modifyInterestRate(0.04);
        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();

        assertTrue(Math.abs(saver1.getSavingsBalance() - 2006.666) < 0.001);
        assertTrue(Math.abs(saver2.getSavingsBalance() - 3010) < 0.001);

        SavingsAccount.modifyInterestRate(0.05);
        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();

        assertTrue(Math.abs(saver1.getSavingsBalance() - 2015.027) < 0.001);
        assertTrue(Math.abs(saver2.getSavingsBalance() - 3022.541) < 0.001);
    }
}
