package lecture2_classes;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class TestSortTask {

    @Test
    public void testAverageCase() {
        int[] arr = new int[] {9, 0, -1, 8, 4, 10, -5, -11, 2, 8};
        int[] sorted = new int[] {-11, -5, -1, 0, 2, 4, 8, 8, 9, 10};
        SortTask.quickSort(arr, 0, arr.length - 1);

        assertArrayEquals(sorted, arr);
    }

    @Test
    public void testWorstCase() {
        int[] arr = new int[] {10, 9, 8, 8, 4, 2, 0, -1, -5, -11};
        int[] sorted = new int[] {-11, -5, -1, 0, 2, 4, 8, 8, 9, 10};

        SortTask.quickSort(arr, 0, arr.length - 1);

        assertArrayEquals(sorted, arr);
    }

    @Test
    public void testBestCase() {
        int[] arr = new int[] {-11, -5, -1, 0, 2, 4, 8, 8, 9, 10};
        int[] sorted = new int[] {-11, -5, -1, 0, 2, 4, 8, 8, 9, 10};

        SortTask.quickSort(arr, 0, arr.length - 1);

        assertArrayEquals(sorted, arr);
    }
}
