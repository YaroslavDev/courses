package lecture9_threads;

import org.junit.Test;

public class TestProdCons {

    @Test
    public void testProdCons() throws Exception {
        Stock c = new Stock();
        Producer p1 = new Producer(c, 1);
        Consumer c1 = new Consumer(c, 1);

        p1.start();
        c1.start();

        p1.join();
        c1.join();
    }
}
