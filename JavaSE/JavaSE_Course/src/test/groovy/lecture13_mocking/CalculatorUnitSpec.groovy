package lecture13_mocking

import spock.lang.Specification

class CalculatorUnitSpec extends Specification {

    Calculator calculator
    NumberProvider numberProvider
    Random random

    def setup() {
        calculator = new Calculator()
        numberProvider = Mock(NumberProvider)
        random = new Random()
    }

    def "add should add 2 numbers"() {
        int a = 1
        int b = 2
        expect:
            calculator.add(a, b) == 3
    }

    def "provider should provide 2 numbers for calculator"() {
        when:
            int a = numberProvider.getNumber()
            int b = numberProvider.getNumber()

        then:
            2 * numberProvider.getNumber() >> random.nextInt()

        expect:
            calculator.add(a, b) == a + b
    }
}
